Source: libperl-prereqscanner-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcpan-meta-requirements-perl <!nocheck>,
                     libgetopt-long-descriptive-perl <!nocheck>,
                     libmodule-path-perl <!nocheck>,
                     libmoo-perl <!nocheck>,
                     libnamespace-autoclean-perl <!nocheck>,
                     libparams-util-perl <!nocheck>,
                     libppi-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libstring-rewriteprefix-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtry-tiny-perl <!nocheck>,
                     libtype-tiny-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libperl-prereqscanner-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libperl-prereqscanner-perl.git
Homepage: https://metacpan.org/release/Perl-PrereqScanner
Rules-Requires-Root: no

Package: libperl-prereqscanner-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcpan-meta-requirements-perl,
         libgetopt-long-descriptive-perl,
         libmodule-path-perl,
         libmoo-perl,
         libnamespace-autoclean-perl,
         libparams-util-perl,
         libppi-perl,
         libscalar-list-utils-perl,
         libstring-rewriteprefix-perl,
         libtype-tiny-perl
Description: module for extracting prerequisites from Perl code
 Perl::PrereqScanner is a Perl module that extracts distribution prerequisites
 from files through various means. The extraction is not perfect but provides
 a best-guess approximation.
 .
 It currently detects the following:
  * plain lines beginning with use or require in perl modules and scripts
  * regular inheritance declared with the base and parent pragmata
  * Moose inheritance declared with the extends keyword
  * Moose roles included with the with keyword
